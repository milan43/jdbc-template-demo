package com.demo.jdbctemplate;

import com.demo.jdbctemplate.daoimpl.JdbcDaoImpl;
import com.demo.jdbctemplate.domain.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

/*

 @Author melone
 @Date 6/28/18 
 
 */
public class JdbcApp {
    public static void main(String[] args) throws SQLException {
        ApplicationContext context = new ClassPathXmlApplicationContext("resources/config/config.xml");
        JdbcDaoImpl dao = ((JdbcDaoImpl) context.getBean("jdbcdaoimpl"));
       // dao.addEmployee(new Employee("Asheki", 2002300.00));
        System.out.println(dao.employeeList());
        System.out.println(dao.findName(1));
        //System.out.println(dao.employeeList());
        /*use spring-config file for messaging
        message key, parameter, default message, locale
        context.getMessage("greeting", null, "Default Hi", null);
        domain.addEmployee(new Employee("Hari", 20000.00));*/
    }
}
