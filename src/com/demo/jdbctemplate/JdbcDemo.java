package com.demo.jdbctemplate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*

 @Author melone
 @Date 7/2/18 
 
 */
public class JdbcDemo {
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String URL = "jdbc:mysql://localhost:3307/jdbctest";
    private static final String SQL = "INSERT INTO EMPLOYEE VALUES(3,'Hari',20000.00)";
    public static void main(String[] args) throws SQLException {
        try(Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)){
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.executeUpdate();
        }
    }

}
