package com.demo.jdbctemplate.daoimpl;

import com.demo.jdbctemplate.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;


/*

 @Author melone
 @Date 6/28/18 
 
 */

public class JdbcDaoImpl {
    private static final Logger LOGGER = Logger.getLogger("JdbcDaoImpl.class");
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * @return number of rows in employee table
     */
    public int countRows() {
        String sql = "SELECT COUNT(*) FROM EMPLOYEE";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    /**
     * @param empid id of employee
     * @return name of employee havind e_id = id
     */
    public String findName(int empid) {
        String sql = "SELECT E_NAME FROM employee WHERE E_ID = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{empid}, String.class);
    }

    /**
     * @param empid
     * @return object of employee with retrieved value
     */
    public Employee employeeById(int empid) {
        String sql = "SELECT * FROM employee WHERE E_ID = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{empid}, new EmployeeMapper());
    }

    /**
     * @return List of Employees available in employee table
     */
    public List<Employee> employeeList() {
        String sql = "SELECT * FROM employee";
        return jdbcTemplate.query(sql, new EmployeeMapper());
    }

    /**
     * Adds new employee to table values provided through main class
     * @see com.demo.jdbctemplate.JdbcApp
     * @param employee
     */
    public void addEmployee(Employee employee) {
        String sql = "INSERT INTO employee(E_NAME, E_SALARY) VALUES(?,?)";
        jdbcTemplate.update(sql, new Object[]{ employee.getName(), employee.getSalary()});
    }

    /**
     * EmployeeMapper is an inner class implementing RowMapper interface of Employee type
     */
    private static final class EmployeeMapper implements RowMapper<Employee> {
        /**
         *
         * @param rs resultset optained from retrieve operation
         * @param numRow number of rows to be added we can leave it
         * @return List<Employee> it can be single employee also
         * @throws SQLException
         */
        @Override
        public Employee mapRow(ResultSet rs, int numRow) throws SQLException {
            Employee emp = new Employee();
            emp.setId(rs.getInt("E_ID"));
            emp.setName(rs.getString("E_NAME"));
            emp.setSalary(rs.getDouble("E_SALARY"));
            return emp;
        }
    }
}
